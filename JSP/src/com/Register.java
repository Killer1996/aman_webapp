package com;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Register
 */
public class Register extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Register() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String Account=request.getParameter("n1");
		String name=request.getParameter("n2");
		String DoB=request.getParameter("n3");
		String BName=request.getParameter("n4");
		String Ifsc=request.getParameter("n5");
		String phone=request.getParameter("n6");
		String bal=request.getParameter("n7");
		long b= Long.parseLong(phone);
		
		
		
		try
		{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@localHost:1521:xe","system","tiger");
			PreparedStatement pstmt= conn.prepareStatement("insert into phoneRegss values(?,?,?,?,?,?,?)");
			pstmt.setInt(1, Integer.parseInt(Account));
			pstmt.setString(2, name);
			pstmt.setString(3, DoB);
			pstmt.setString(4, BName);
			pstmt.setString(5, Ifsc);
			pstmt.setLong(6, b);
			pstmt.setInt(7, Integer.parseInt(bal));
			pstmt.execute();
			conn.close();
		}
		catch(Exception e)
		{
			out.print(e);
		}
		RequestDispatcher rd = request.getRequestDispatcher("practice.html");
		  rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
