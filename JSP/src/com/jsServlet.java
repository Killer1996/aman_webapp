package com;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class jsServlet
 */
public class jsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public jsServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		response.setContentType("text/html");
		String s = request.getParameter("n1");
		//int id = Integer.parseInt(s);
		
		AdressTo add = new AdressTo();
		add.setArea("Telipara");
		add.setCity("Bilaspur");
		add.setCountry("India");
		
		
		
		jspppp jp = new jspppp();
		jp.setSID(s);
		jp.setName("aman");
		jp.setPh("56789");
		jp.setAdd(add);
		HttpSession se = request.getSession();
		se.setAttribute("jpp", jp);
		
		//se.setAttribute("Add", add);
		RequestDispatcher rd = request.getRequestDispatcher("Jsp/LEL.jsp");
		rd.forward(request, response);
		
	
	
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
