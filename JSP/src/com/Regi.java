package com;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Regi
 */
public class Regi extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Regi() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String Fname=request.getParameter("FN");
		String Lname=request.getParameter("LN");
		String pass=request.getParameter("pass");
		String email=request.getParameter("email");
		String age=request.getParameter("age");
		String ph=request.getParameter("num");
		String g=request.getParameter("m");
		int a=Integer.parseInt(age);
		long gg=Long.parseLong(ph);
		
		out.print("Sucessfully Register.....");
		out.print("<a href=Login.html>Login </a>");
		
		try
		{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@localHost:1521:xe","system","tiger");
			PreparedStatement pstmt= conn.prepareStatement("insert into Regi values(?,?,?,?,?,?,?)");
			pstmt.setString(1, Fname);
			pstmt.setString(2, Lname);
			pstmt.setString(3, pass);
			pstmt.setString(4, email);
			pstmt.setInt(5, a);
			pstmt.setLong(6, gg);
			pstmt.setString(7, g);
			pstmt.execute();
			conn.close();
		}
		catch(Exception e)
		{
			out.print(e);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
