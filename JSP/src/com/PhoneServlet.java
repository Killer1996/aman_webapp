package com;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class DemoServlet1
 */
public class PhoneServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PhoneServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/HTML");
		String name=request.getParameter("n1");
		String pass = request.getParameter("n2");
		System.out.println(name);
		PrintWriter out=response.getWriter();
		try
		{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@localHost:1521:xe","system","tiger");
			PreparedStatement pstmt = conn.prepareStatement("select email,pass from Regi where email=? AND pass=?");
			pstmt.setString(1, name);
			pstmt.setString(2, pass);
			ResultSet rs = pstmt.executeQuery();
		
		
		
		if(rs.next()==true)
		{
			HttpSession session = request.getSession(true);
			session.setAttribute("user", name);
			String s = (String)session.getAttribute("user");
			System.out.println(s);
			out.print(s);
			RequestDispatcher rd = request.getRequestDispatcher("/practice.html");
			rd.include(request, response);
		
		}
		else
		{
			out.print("Record not found......");
			RequestDispatcher rd = request.getRequestDispatcher("Login.html");
			  rd.include(request, response);
		}
		
	}
		catch(Exception e)
		{
			out.print(e);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
