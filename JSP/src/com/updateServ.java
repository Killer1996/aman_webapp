package com;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class MainServ
 */
public class updateServ extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public updateServ() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String id=request.getParameter("n1");
		String name=request.getParameter("n2");
		String age=request.getParameter("n3");
		String desi=request.getParameter("n4");
		String sal=request.getParameter("n5");
	
		
	HttpSession session = request.getSession();
	long ls=session.getLastAccessedTime();
	Date d = new Date(ls);
	SimpleDateFormat sd = new SimpleDateFormat("hh:mm:ss a");
	String ls2=sd.format(d);
		
		try
		{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@localHost:1521:xe","system","tiger");
			PreparedStatement pstmt= conn.prepareStatement("update jspMainEE set name=?,age=?,salary=?,designation=?,lastseen=? where id=?");
			pstmt.setString(1, name);
			pstmt.setInt(2, Integer.parseInt(age));
			pstmt.setInt(3, Integer.parseInt(sal));
			pstmt.setString(4, desi);
			pstmt.setString(5, ls2);
			pstmt.setInt(6, Integer.parseInt(id));
			pstmt.execute();
			conn.close();
		}
		catch(Exception e)
		{
			out.print(e);
		}
		RequestDispatcher rd = request.getRequestDispatcher("DisJSP.jsp");
		  rd.include(request, response);		
		  
	
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
