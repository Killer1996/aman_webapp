package com;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Balance
 */
public class Balance extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Balance() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/HTML");
		String name=request.getParameter("n1");
		PrintWriter out=response.getWriter();
		try
		{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@localHost:1521:xe","system","tiger");
			PreparedStatement pstmt = conn.prepareStatement("select * from phoneRegss where Account=? or phone=?");
			pstmt.setString(1, name);
			pstmt.setString(2, name);
			ResultSet rs = pstmt.executeQuery();
		
		
		if(rs.next()==true)
		{
		 int bal=rs.getInt(7);
		 // request.setAttribute("n2", bal);
		 // RequestDispatcher rd = request.getRequestDispatcher("Balance.html");
		//  rd.include(request, response);
		 out.print("<script type=\"text/javascript\">");
			out.print("alert(\'your Balance is "+bal+"\');");
			 out.print("</script>");
			RequestDispatcher rd = request.getRequestDispatcher("practice.html");
			  rd.include(request, response);
		
		}
		else
		{
			
			out.print("<script type=\"text/javascript\">");
			out.print("alert(\"record not found \');");
			RequestDispatcher rd = request.getRequestDispatcher("practice.html");
			  rd.include(request, response);
		}
		
	}
		catch(Exception e)
		{
			out.print(e);
		}

		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
